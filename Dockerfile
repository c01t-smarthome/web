FROM node:11-alpine as builder

RUN mkdir -p /app

WORKDIR /app

COPY . .

RUN npm install


FROM node:11-alpine as maker

COPY --from=builder /app /app

WORKDIR /app

RUN npm run build


FROM nginx:alpine

COPY nginx.conf /etc/nginx/nginx.conf

COPY --from=maker /app/dist/ui /srv

EXPOSE 4200
