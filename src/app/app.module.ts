import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';

import {NgDragDropModule} from 'ng-drag-drop';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {DashboardComponent} from './components/dashboard.component';
import {DeviceSettingsComponent} from './components/device-settings.component';

import {BatteryLevelComponent} from './components/battery-level.component';
import {DoubleValueComponent} from './components/double-value.component';
import {IconDeviceComponent} from './components/icon-device.component';
import {ScaleSetComponent} from './components/scale-set.component';
import {SingleValueComponent} from './components/single-value.component';
import {TimeSinceComponent} from './components/time-since.component';
import {ValueScaleComponent} from './components/value-scale.component';
import {DeviceComponent} from './components/device.component';

import {ZoneService} from './services/zone.service';
import {DeviceService} from './services/device.service';
import {AttributeService} from './services/attribute.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDialogModule,
  MatDividerModule, MatGridListModule, MatInputModule,
  MatSelectModule, MatTabsModule
} from '@angular/material';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {library} from '@fortawesome/fontawesome-svg-core';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {far} from '@fortawesome/free-regular-svg-icons';

import { ChartsModule } from 'ng2-charts';
import {ScaleComponent} from './components/scale.component';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgDragDropModule.forRoot(),
    BrowserAnimationsModule,
    FontAwesomeModule,
    ChartsModule,

    MatTabsModule,
    MatInputModule,
    MatGridListModule,
    MatDividerModule,
    MatCardModule,
    MatDialogModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSelectModule,

    AppRoutingModule,
  ],

  declarations: [
    AppComponent,

    DashboardComponent,

    DeviceComponent,
    DeviceSettingsComponent,

    BatteryLevelComponent,
    DoubleValueComponent,
    IconDeviceComponent,
    ScaleSetComponent,
    ScaleComponent,
    SingleValueComponent,
    TimeSinceComponent,
    ValueScaleComponent,
  ],

  providers: [
    ZoneService,
    DeviceService,
    AttributeService,
  ],

  bootstrap: [
    AppComponent
  ],

  entryComponents: [
    DeviceSettingsComponent,
  ]
})
export class AppModule {
  constructor() {
    library.add(fas, far);
  }
}
