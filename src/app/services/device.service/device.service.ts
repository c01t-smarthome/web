import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {IToggleAction, IValueAction} from '../../components/scale-set.component';

export interface IAttribute {
  name: string;
  title: string;
  scale: string;
  precision?: number;
  monotone?: boolean;
  expectedMin?: number;
  expectedMax?: number;
  lambda?: string;
  sign?: boolean;
  virtual?: boolean;
}

export interface IWidget {
  prime: IAttribute;
  secondary?: IAttribute;
  state?: IAttribute;
  action?: IToggleAction | IValueAction;
}

export interface IDeviceModel {
  uid: string;
  tag: string;
  name: string;
  zone: string | null;
  assets: Record<string, string>;
  icon: IWidget;
  widgets: IWidget[];
  attributes: Record<string, any>;
}

interface IDeviceUpdate {
  device: string;
  key: string;
  lastUpdated: string;
  value: any;
}

@Injectable()
export class DeviceService {

  private devicesSubject: BehaviorSubject<IDeviceModel[]> = new BehaviorSubject<IDeviceModel[]>(
    JSON.parse(window.localStorage.getItem('devices') || '[]')
  );

  public get devices(): Observable<IDeviceModel[]> {
    return this.devicesSubject;
  }

  constructor(
    private http: HttpClient,
  ) {
    const source = new EventSource('/api/v1/events');
    source.addEventListener('error', console.error);
    source.addEventListener('open', console.log);
    source.addEventListener('reported', ({data}: any) => {
      const {
        device,
        key,
        lastUpdated,
        value
      }: IDeviceUpdate = JSON.parse(data);

      this.devicesSubject.value
        .filter(({uid}) => uid === device)
        .forEach(x => {
          x.attributes[key] = value;
          x.attributes['last-updated'] = lastUpdated;
        });

      this.persistAndPublish([
        ...this.devicesSubject.value
      ]);
    });
  }

  public refresh(): void {
    this.http
      .get<IDeviceModel[]>('/api/v1/device')
      .subscribe(this.persistAndPublish);
  }

  public put(uid: string, data: Partial<IDeviceModel>): void {
    this.http
      .put<IDeviceModel>(`/api/v1/device/${uid}`, data)
      .subscribe(this.updateAndPublish);
  }

  public query(uid: string, attributes: string[], since: Date, till: Date, interval: string): Observable<any> {
    return this.http
      .get<any>(`/api/v1/device/${uid}/history`
        + `?since=${since.toISOString()}`
        + `&till=${till.toISOString()}`
        + `&interval=${interval}`
        + attributes.map(a => `&attributes[]=${a}`).join(''));
  }

  private updateAndPublish = (device: IDeviceModel) => {
    const devices = this.devicesSubject.value;

    devices
      .filter(({uid: _}) => device.uid !== _)
      .forEach((original) => {
        Object.keys(device).forEach(prop => {
          original[prop] = device[prop];
        });
      });
    this.persistAndPublish(devices);
  }

  private persistAndPublish = (devices: IDeviceModel[]) => {
    window.localStorage.setItem('devices', JSON.stringify(devices));
    this.devicesSubject.next(JSON.parse(window.localStorage.getItem('devices')));
  }
}
