import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';

export interface IZoneModel {
  uid: string;
  tag: string;
  name: string;
  color: string | null;
}

@Injectable()
export class ZoneService {

  private zonesSubject: BehaviorSubject<IZoneModel[]> = new BehaviorSubject<IZoneModel[]>(
    JSON.parse(window.localStorage.getItem('zones') || '[]')
  );

  public get zones(): Observable<IZoneModel[]> {
    return this.zonesSubject;
  }

  constructor(
    private http: HttpClient,
  ) {}

  public refresh(): void {
    this.http
      .get<IZoneModel[]>('/api/v1/zone')
      .subscribe(this.persistAndPublish);
  }

  private persistAndPublish = (zones: IZoneModel[]) => {
    window.localStorage.setItem('zones', JSON.stringify(zones));
    this.zonesSubject.next(zones);
  }
}
