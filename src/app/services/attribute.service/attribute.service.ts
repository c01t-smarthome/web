import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IAttribute} from '../device.service';

export interface IListenSetting {
  kind: 'listen';
  attribute: string;
}

export interface IRepublishSetting {
  kind: 'republish';
  attribute: string;
}

export interface IScheduleSetting {
  kind: 'schedule';
  schedule: {[key: string]: string|number|boolean};
}

export type ISetting = IRepublishSetting | IScheduleSetting | IListenSetting;

export interface IAttributeModel {
  uid: string;
  key: string;
  title: string;
  kind: 'in' | 'out';
  definition: IAttribute;
  setting: ISetting | null;
  value: any;
}

@Injectable()
export class AttributeService {

  constructor(
    private http: HttpClient,
  ) {
  }

  public list(device: string): Observable<IAttributeModel[]> {
    return this.http
      .get<IAttributeModel[]>(`/api/v1/device/${device}/attribute`);
  }

  public put(device: string, uid: string, data: Partial<IAttributeModel>): Observable<IAttributeModel> {
    return this.http
      .put<IAttributeModel>(`/api/v1/device/${device}/attribute/${uid}`, data);
  }
}
