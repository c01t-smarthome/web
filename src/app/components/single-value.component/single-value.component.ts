import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-single-value',
  templateUrl: './single-value.component.html',
  styleUrls: ['./single-value.component.scss'],
})
export class SingleValueComponent {
  @Input() public active: boolean | number = null;
  @Input() public height = 64;
  @Input() public width = 64;
  @Output() public action = new EventEmitter();

  private get borderRadius(): number {
    return Math.floor(Math.min(this.width, this.height) / 2);
  }
}
