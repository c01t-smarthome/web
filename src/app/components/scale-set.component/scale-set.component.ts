import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IScaleSettings} from '../scale.component';

export interface IValueAction {
  kind: 'value';
  attribute: string;
  value: any;
}
export interface IToggleAction {
  kind: 'toggle';
  attribute: string;
}

export interface IScaleSetSettings {
  state?: IScaleSettings;
  prime: IScaleSettings;
  secondary?: IScaleSettings;
  action?: IToggleAction | IValueAction;
}

@Component({
  selector: 'app-scale-set',
  templateUrl: './scale-set.component.html',
  styleUrls: ['./scale-set.component.sass'],
})
export class ScaleSetComponent {
  @Input() public scales: IScaleSetSettings[];
  @Input() public assets: Record<string, string> = null;
  @Output() public action = new EventEmitter();

  public handleClick(scale: IScaleSetSettings) {
    if (!scale.action) {
      return;
    }

    switch (scale.action.kind) {
      case 'toggle':
      case 'value':
        this.action.emit(scale.action);
        return;
    }
  }
}
