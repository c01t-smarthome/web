import {Component, Input} from '@angular/core';
import {Scale} from '../value-scale.component';

export interface IScaleSettings {
  name?: string;
  precision?: number;
  scale: Scale;
  sign?: boolean;
  value: any;
}

@Component({
  selector: 'app-scale',
  templateUrl: './scale.component.html',
})
export class ScaleComponent {
  @Input() public scale: IScaleSettings;
  @Input() public assets: Record<string, string> = null;
}
