import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {DeviceService, IDeviceModel} from '../../services/device.service';
import {AttributeService, IAttributeModel} from '../../services/attribute.service';
import {forkJoin} from 'rxjs';
import {map} from 'rxjs/operators';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {BaseChartDirective, Color, Label} from 'ng2-charts';
import {ChartDataSets, ChartOptions} from 'chart.js';
import {IScaleSettings} from '../scale.component';

const COLORS = ('d50000 f28a02 ffc107 ffd600 78bd0b 1d8e17 0091ea 0097a7' +
  ' 467d97 01579b 673ab7 c51162 880e4f ad1457 6a1b9a 4a148c' +
  ' 3f51b5 d500f9 00bcd4 00c853 2eb966 820404 f23502 075f76 ff6900'
)
  .split(' ')
  .map(x => '#' + x)
  .reverse();

const CHART_COLORS = COLORS
  .map(x => [
    parseInt(x.substr(1, 2), 16),
    parseInt(x.substr(3, 2), 16),
    parseInt(x.substr(5, 2), 16),
  ].join(','))
  .map(color => ({ // grey
    backgroundColor: `rgba(${color},0.2)`,
    borderColor: `rgba(${color},1)`,
    pointBackgroundColor: `rgba(${color},1)`,
    pointHoverBorderColor: `rgba(${color},0.8)`
  }));

@Component({
  selector: 'app-device-setting',
  templateUrl: './device-settings.component.html',
  styleUrls: ['./device-settings.component.sass']
})
export class DeviceSettingsComponent implements OnInit {

  public attributes: IAttributeModel[] | null = [];
  public active: 0 | 1 | 2 | 3 | 4 = 4;

  public lineChartData: ChartDataSets[] = [];
  public lineChartLabels: Label[] = [];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      xAxes: [{}],
      yAxes: [],
    },
    annotation: {
      annotations: [],
    },
  };
  public lineChartColors: Color[] = CHART_COLORS;
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  constructor(
    public dialogRef: MatDialogRef<DeviceSettingsComponent>,
    @Inject(MAT_DIALOG_DATA) public device: IDeviceModel,
    private attributeService: AttributeService,
    private deviceService: DeviceService,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    this.attributeService
      .list(this.device.uid)
      .subscribe(attributes => {
        this.attributes = attributes;
        this.query().subscribe(this.updateChart);
      });
  }

  public async changeTab({index}) {
    this.active = index;
    if (this.attributes) {
      this.query().subscribe(this.updateChart);
    }
  }

  public handleRePublish(attribute: IAttributeModel, {checked}: any) {
    attribute.setting = !checked ? null : {
      kind: 'republish',
      attribute: attribute.key,
    };
  }

  public handleInRouting(attribute: IAttributeModel, {value}: any) {
    switch (value) {
      case null:
        attribute.setting = null;
        break;
      case 'value':
        attribute.setting = {
          kind: 'value',
          value: attribute.value,
        } as any;
        break;
      case 'listen':
        attribute.setting = {
          kind: 'listen',
          attribute: attribute.key,
        };
        break;
      case 'schedule':
        attribute.setting = {
          kind: 'schedule',
          schedule: {},
        };
        break;
    }
  }

  public getScaleFor({key, definition}: IAttributeModel): IScaleSettings {
    const {scale, name, precision, sign} = definition;
    return {
      scale,
      name,
      precision: (precision || 0) + 1,
      sign,
      value: this.device.attributes[key],
    };
  }

  public close() {
    this.dialogRef.close();
  }

  public forget() {
    this.dialogRef.close();
  }

  public save() {
    forkJoin([
      ...this.attributes
        .filter(({kind}) => kind === 'out')
        .map(({uid, setting}) => this.attributeService
          .put(this.device.uid, uid, {
            setting
          })),
      ...this.attributes
        .filter(({kind}) => kind === 'in')
        .map(({uid, setting}: any) => this.attributeService
          .put(this.device.uid, uid, {
            setting: setting && setting.kind === 'value' ? undefined : setting,
            value: setting && setting.kind === 'value' ? getValue(setting.value) : undefined,
          }))
      ]
    )
      .subscribe(() => this.dialogRef.close());
  }

  private query() {
    let till = new Date(new Date().getTime() + 60 * 1000);
    let since = new Date(new Date().getTime() - 60 * 60 * 1000);
    let interval = '2m';
    switch (this.active) {
      case 3:
        till = new Date(new Date().getTime() + 60 * 1000);
        since = new Date(new Date().getTime() - 30 * 24 * 60 * 60 * 1000);
        interval = '1d';
        break;

      case 2:
        till = new Date(new Date().getTime() + 60 * 1000);
        since = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
        interval = '8h';
        break;

      case 1:
        till = new Date(new Date().getTime() + 60 * 1000);
        since = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
        interval = '1h';
        break;
    }

    const attributes = this.attributes
      .filter(({kind}) => kind === 'out')
      .filter(({definition}) => !definition.virtual)
      .filter(({definition}) => definition.scale !== 'timestamp')
      .map(({key}) => key);

    return this.deviceService.query(this.device.uid, attributes, since, till, interval);
  }

  private updateChart = (stats: any) => {

    console.log(stats);

    let labelFormat: any = { minute: '2-digit', hour: '2-digit', hourCycle: 'h24'};
    switch (this.active) {
      case 3:
        labelFormat = { month: 'short', day: '2-digit' };
        break;

      case 2:
        labelFormat = { hour: '2-digit', weekday: 'short',  minute: '2-digit', hourCycle: 'h24'};
        break;
    }

    this.lineChartLabels = stats[0].data
      .map(({timestamp}) => new Date(timestamp).toLocaleString('en-US', labelFormat));

    this.lineChartData = stats.map(({definition, data}) => ({
      yAxisID: definition.scale,
      label: definition.title,
      data: data.map(({value}) => value || null),
    }));

    this.lineChartOptions = {
      responsive: true,
      scales: {
        // We use this empty structure as a placeholder for dynamic theming.
        xAxes: [{}],
        yAxes: this.attributes
          .filter(({kind}) => kind === 'out')
          .filter(({definition}) => !definition.virtual)
          .filter(({definition}) => definition.scale !== 'timestamp')
          .map(({definition}) => definition.scale)
          .reduce((a, name) => {
            if (a.indexOf(name) < 0) {
              a.push(name);
            }

            return a;
          }, [])
          .map(scale => ({
            id: scale,
            type: scale === 'RYG' ? 'category' : 'linear',
            labels: scale === 'RYG' ? ['Lo', 'Mid', 'Hi'] : undefined,
            position: scale !== 'bit' && scale !== 'RYG' ? 'left' : 'right',
            steppedLine: scale === 'bit' || scale === 'RYG' ? 'after' : false,
            scaleLabel: {
              display: true,
              labelString: scale,
            },
            ticks: {
              beginAtZero: scale === 'bit' || scale === 'RYG',
              precision: scale === 'bit' || scale === 'RYG' ? 0 : 2,
            },
          }))
      },
      annotation: {
        annotations: [{}],
      },
    };
  }
}

function getValue(v: string): string | number {
  let x = parseFloat(v);
  if (!isNaN(x)) {
    return x;
  }
  x = parseInt(v, 10);
  if (!isNaN(x)) {
    return x;
  }

  return v;
}
