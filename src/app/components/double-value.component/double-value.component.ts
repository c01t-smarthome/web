import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-double-value',
  templateUrl: './double-value.component.html',
  styleUrls: ['./double-value.component.scss'],
})
export class DoubleValueComponent {
  @Input() public active: boolean | number = null;
  @Input() public height = 64;
  @Input() public width = 64;
  @Output() public action = new EventEmitter();

  private get borderRadius(): number {
    return Math.floor(Math.min(this.width, this.height) / 2);
  }
}
