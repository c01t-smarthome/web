import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IAttribute, IDeviceModel, IWidget} from '../../services/device.service';
import {IScaleSetSettings} from '../scale-set.component';
import {IScaleSettings} from '../scale.component';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.sass']
})
export class DeviceComponent {

  @Output() settings = new EventEmitter();
  @Output() action = new EventEmitter();

  @Input()
  public busy = false;
  public initialized: boolean;
  public assets: Record<string, string>;
  public lastUpdated: Date;
  public scales: IScaleSetSettings[];
  public batteryLevel: 0 | 1 | 2 | null;

  public title: string;
  public tag: string;

  @Input()
  public set device(value: IDeviceModel) {
    const date = new Date(value.attributes['last-updated'] || 0);
    this.title = value.name;
    this.tag = value.tag;
    this.initialized = Math.abs(date.getTime() - Date.now()) < 600 * 1000;
    this.assets = value.assets;
    this.lastUpdated = date;
    this.batteryLevel = value.attributes['battery-level'];
    this.scales = value.widgets.map(({prime, secondary, state, action}: IWidget) => ({
      prime: prime ? makeScale(prime, value.attributes) : undefined,
      secondary: secondary ? makeScale(secondary, value.attributes) : undefined,
      state: state ? makeScale(state, value.attributes) : undefined,
      action,
    }));
  }
}

function makeScale(
  {lambda, name, precision, scale, sign}: IAttribute,
  values: Record<string, any>
): IScaleSettings {
  return {
    name,
    precision,
    scale,
    sign,
    value: values[name],
  };
}
