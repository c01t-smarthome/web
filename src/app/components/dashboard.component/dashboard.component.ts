import {DeviceService, IDeviceModel, IZoneModel, ZoneService} from 'src/app/services';
import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {DeviceSettingsComponent} from '../device-settings.component';
import {AttributeService} from '../../services/attribute.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  public zones: IZoneModel[];
  public devices: IDeviceModel[];

  constructor(
    public dialog: MatDialog,
    private zoneService: ZoneService,
    private deviceService: DeviceService,
    private attributeService: AttributeService,
  ) {}

  public ngOnInit() {
    this.zoneService
      .zones
      .subscribe((zones) => this.zones = [
        ...zones, {
          name: 'Unassigned',
          uid: null,
          tag: null,
        } as IZoneModel]);

    this.deviceService
      .devices
      .subscribe((devices) => this.devices = devices);
  }

  public handleDrop({dragData}: {dragData: IDeviceModel}, zone: IZoneModel) {
    // Get the dropped data here
    console.log(dragData);

    const device: any = this.devices.find(({uid}) => uid === dragData.uid);
    device.zone = zone.uid;
    device._busy = true;

    this.deviceService.put(dragData.uid, {
      zone: zone.uid,
    });
  }

  public handleSettings(data: IDeviceModel) {
    this.dialog
      .open(DeviceSettingsComponent, {data, width: '900px'})
      .afterClosed()
        .subscribe(() => console.log('done'));
  }

  public async handleAction({uid, attributes}: IDeviceModel, {value, kind, attribute}: any) {
    if (kind === 'toggle') {
      value = attributes[attribute] ? 0 : 1;
    }

    const attributesDetails = await this.attributeService.list(uid).toPromise();

    const [model] = attributesDetails
      .filter(({kind: _}) => _ === 'in')
      .filter(({key: _}) => _ === attribute);

    await this.attributeService
      .put(uid, model.uid, {
        value
      })
      .toPromise();
  }
}
