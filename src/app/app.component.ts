import {Component, OnInit} from '@angular/core';
import {ZoneService} from './services/zone.service';
import {DeviceService} from './services/device.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'Smarthome';

  constructor(
    private zoneService: ZoneService,
    private deviceService: DeviceService,
  ) {
  }

  ngOnInit(): void {
    this.zoneService.refresh();
    this.deviceService.refresh();
  }
}
